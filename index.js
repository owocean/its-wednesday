const Discord = require('discord.js');
const client = new Discord.Client();
const cron = require('node-cron');
require('dotenv').config();

client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`);
    cron.schedule('0 12 * * *', () => {
        let date = new Date();
        let day = date.getDay();
        client.channels.cache.get(process.env.CHANNEL_ID).send({
            files: [{
                attachment: 'days/' + day + '.png',
                name: 'its-wednesday.png'
            }]
        });
    });
});

client.on('message', msg => {
    if (msg.mentions.has(client.user)) {
        let date = new Date();
        let day = date.getDay();
        msg.channel.send({
            files: [{
                attachment: 'days/' + day + '.png',
                name: 'its-wednesday.png'
            }]
        });
    }
});

client.login(process.env.TOKEN);